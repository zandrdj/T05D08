#include <stdio.h>
#include <math.h>
#define NMAX 10

int input(int *a, int *n);
void output(int *a, int *n);
int max(int *a, int *n);
int min(int *a, int *n);
double mean(int *a, int *n);
double variance(int *a, int *n);

void output_result(int max_v,
                   int min_v,
                   double mean_v,
                   double variance_v);

int main() {
    int n, data[NMAX];
    if ( input(data, &n) == 0 /*|| n % 1 == 0 */) return 0;
    output(data, &n);
    output_result(max(data, &n),
                  min(data, &n),
                  mean(data, &n),
                  variance(data, &n));

    return 0;
}

int input(int *a, int *n) {
    if ( scanf("%d", n) != 1 || *n <= 0 || *n > NMAX || *n % 1 != 0 ) {printf("n/a"); return 0;}
    else
    {for ( int *p = a; p - a < *n; p++ ) {
        if ( scanf("%d", p) != 1  ) {
            printf("n/a");
       break; }
    }
    }
    return 1;
}

void output(int *a, int *n) {
    for ( int *p = a; p - a < *n; p++ ) printf("%d ", *p);
    printf("\n");
}

int max(int *a, int *n) {
    int max_v = a[1];
    for ( int *p = a; p - a < *n; p++ ) {
        if ( *p > max_v )
            max_v = *p;
    }
    return max_v;
}

int min(int *a, int *n) {
    int *min_v = &a[0];
    for ( int i = 0; i < *n; i++ ) {
        if ( a[i] < *min_v )
            *min_v = a[i];
    }
    return *min_v;
}

double mean(int *a, int *n) {
    double mean_v = 0, M = 0;
    for ( int i = 0; i < *n; i++ ) {
        M += a[i];
    }
    mean_v = M / *n;
    return mean_v;
}

double variance(int *a, int *n) {
    double variance_v = 0, V = 0;
    double mean_v = 0, M = 0;
    for ( int i = 0; i < *n; i++ ) {
        M += a[i];
    }
    mean_v = M / *n;
    for ( int i = 0; i < *n; i++ ) {
        V += (a[i] - mean_v) * (a[i] - mean_v);
    }
    variance_v = V / *n;
    return variance_v;
}

void output_result(int max_v, int min_v, double mean_v, double variance_v) {
    printf("%d %d %.6f %.6f ", max_v, min_v, mean_v, variance_v);
}
