// Copyright [19.07.2021] <hvernell>
#include <stdio.h>
#define NMAX 10

int input(int *a, int *n);
void output(int *a, int *n);
void squaring(int *a, int *n);

int main() {
    int n, data[NMAX];
    if ( input(data, &n) == 0 || n % 1 != 0 ) return 0;
    squaring(data, &n);
    output(data, &n);
    return 0;
}

int input(int *a, int *n) {
    if ( scanf("%d", n) != 1 || *n <= 0 || *n > NMAX || *n % 1 != 0) { printf("n/a"); return 0; }
    else
    {for ( int *p = a; p - a < *n; p++ ) {
        if ( scanf("%d", p) != 1 || *p % 1 != 0 ) {
            printf("n/a"); return 0; }
    }
    }
    return 1;}

void output(int *a, int *n) {
    for (int *p = a; p - a < *n; p++) printf("%d ", *p);
}

void squaring(int *a, int *n) {
    for ( int i = 0; i < *n; i++ )
        if ( a[i] % 1 == 0 ) a[i] *= a[i];
}
